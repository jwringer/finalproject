﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterScript : MonoBehaviour {
	public float speed = 10f;
	public float Timer = 3f;
	public GameObject shot;
	public GameObject []shotSpawns;
	// Use this for initialization
	void Start () {
		
		GetComponent<Rigidbody> ().velocity = transform.up * speed;



	}
	
	// Update is called once per frame
	void Update () {
		Timer -= Time.deltaTime;


		if (Timer <= 0f){
			for(int i = 0; i < shotSpawns.Length; i++){
				Instantiate (shot, shotSpawns[i].transform.position, shotSpawns[i].transform.rotation);



			}

			Timer = 3;
		}
	}
}
