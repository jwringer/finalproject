﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftLaunch : MonoBehaviour {
	public GameObject Fighter;
	public GameObject LaunchPos;
	public GameObject UI;
	private LaunchFighter fighterScript;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
	public void ButtonInteract()
	{

		Instantiate(Fighter, LaunchPos.transform.position, LaunchPos.transform.rotation);
		fighterScript = UI.GetComponent<LaunchFighter> ();
		fighterScript.Launching = false;
		Time.timeScale = 1.0F;

	}
}
