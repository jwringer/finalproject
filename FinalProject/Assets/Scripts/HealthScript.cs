﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour {

	public float Health;
	public int CreditsWorth;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Health <= 0) {
			
			GameObject gameController=GameObject.FindWithTag("GameController");
			GameController gameScript = gameController.GetComponent<GameController> ();
			gameScript.Credits = gameScript.Credits + CreditsWorth;



			Destroy(gameObject);

		}
			




	}
}
