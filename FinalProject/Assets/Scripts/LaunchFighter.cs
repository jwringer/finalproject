﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchFighter : MonoBehaviour {

	public bool Launching = false;
	public GameObject Fighter;
	private int Credits;
	private GameController gameController;
	public GameObject LeftLaunch;
	public GameObject RightLaunch;

	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {


			gameController = gameControllerObject.GetComponent <GameController> ();

			Credits = gameController.Credits;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Launching == true) {
			LeftLaunch.SetActive (true);
			RightLaunch.SetActive (true);
		} else {
			LeftLaunch.SetActive (false);
			RightLaunch.SetActive (false);
		}
	}


	public void ButtonInteract()
	{

		Credits = gameController.Credits;
		
		if (Credits >= 5) {
			gameController.Credits = gameController.Credits - 5;
			Launching = true;
			Time.timeScale = 0.0F;
		}
			
	}
}
