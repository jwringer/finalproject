﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretRotate : MonoBehaviour {

	public GameObject shot;
	public GameObject ShotSpawn;
	public float speed = 10f;
	public Transform cannon;
	// Use this for initialization
	void Start () {
		
	}




	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.A)){
			cannon.transform.Rotate(Vector3.forward * Time.deltaTime * 50);

		}

		if (Input.GetKey(KeyCode.D)){
			cannon.transform.Rotate(Vector3.back * Time.deltaTime * 50);

		}

		if (Input.GetKeyDown(KeyCode.Space)) 
		{
			Instantiate(shot, ShotSpawn.transform.position, ShotSpawn.transform.rotation);
		}
	}
}
