﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrigateCall : MonoBehaviour {
	public bool Launching = false;
	public GameObject Frigate;
	private int Credits;
	private GameController gameController;
	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {


			gameController = gameControllerObject.GetComponent <GameController> ();

			Credits = gameController.Credits;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}


	public void ButtonInteract()
	{
		Credits = gameController.Credits;

		if (Credits >= 25) {
			gameController.Credits = gameController.Credits - 25;
			Launching = true;
			Time.timeScale = 0.0F;
		}
	}
}
