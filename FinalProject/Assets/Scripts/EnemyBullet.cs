﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBullet : MonoBehaviour {

	public float Damage;
	public float speed = 50;
	public float destroyTime = 1;
	// Use this for initialization
	void Start () {



		GetComponent<Rigidbody> ().velocity = transform.up * speed;
	}

	// Update is called once per frame
	void Update () {

		destroyTime -= Time.deltaTime;
		if (destroyTime <= 0) {

			Destroy (gameObject);

		}


	}
	void OnTriggerEnter(Collider other)
	{



		if (other.tag == "StationMiddle" || other.tag == "RightPost" || other.tag == "LeftPost" || other.tag == "Ally" ) {
			GameObject gameController=GameObject.FindWithTag("GameController");
			GameController gameScript = gameController.GetComponent<GameController> ();

			if (gameScript.StationHealth > 0) {
				gameScript.StationHealth = gameScript.StationHealth - Damage;
				Destroy (gameObject);
			} else {
				gameScript.HullHealth = gameScript.HullHealth - Damage;

			}


		}

	













		var script = other.GetComponent<HealthScript>();
		if (script == null) {



		} 






		else {

			if (other.tag == "enemy") {


			} else {

				HealthScript healthScript = other.GetComponent<HealthScript> ();
				healthScript.Health = healthScript.Health - Damage;
				Destroy (gameObject);  
			}
		}

	}
}