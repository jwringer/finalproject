﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldRecharge : MonoBehaviour {

	private GameController gameController;
	private int Credits;
	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {


			gameController = gameControllerObject.GetComponent <GameController> ();
			Credits = gameController.Credits;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ButtonInteract(){
		Credits = gameController.Credits;

		if (Credits >= 10) {
			gameController.StationHealth = gameController.StationHealth + 25;

		}
	}
}
