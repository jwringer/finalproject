﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



//Loading scenes^



public class GameController : MonoBehaviour {


	Text shieldtxt;
	Text hullTxt;

	public float Timer = 3;
	public GameObject[] spawnPoints;
	public GameObject[] Enemy;
	GameObject spawnPoint;
	private float reinforce = 0.0f;
	public float shieldRecharge = 5;
	public GameObject Frigate;

	private List<GameObject> selectedEnemies;

	public float StationHealth;
	public float HullHealth;

	public int Credits = 0;
	public float CredTimer = 2;
	public Text CredText;

	public GameObject ShieldText;
	public GameObject HullText;

	private LaunchFighter fighterScript;
	private FrigateCall FrigateScript;
	bool boolLaunch;

	public GameObject UI;

	// Use this for initialization
	void Start () {
		shieldtxt = ShieldText.GetComponent<Text>();
		shieldtxt.text="Shields: " + StationHealth + "%";
		hullTxt = HullText.GetComponent<Text> ();
		hullTxt.text = "Hull: " + HullHealth + "%";
		selectedEnemies = new List<GameObject>();
		fighterScript = UI.GetComponent<LaunchFighter> ();
		FrigateScript = UI.GetComponent<FrigateCall> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (FrigateScript.Launching == true) {
			Time.timeScale = 0.0F;

			if (Input.GetMouseButtonDown (0)) {
				Vector3 spawnPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				spawnPosition.y = 71.0f;

				GameObject objectInstance = Instantiate (Frigate, spawnPosition, Quaternion.Euler (new Vector3 (90, 0, 0)));

				FrigateScript.Launching = false;

			}

		} 
		else {
			Time.timeScale = 1.0F;

		}






		if (Input.GetKey ("escape")) {
			Debug.Log ("quit");
			Application.Quit ();
		}


		if (fighterScript.Launching == true) {
			
			Time.timeScale = 0.0F;


		}



		Timer -= Time.deltaTime;
		CredTimer -= Time.deltaTime;
		if (Timer <= 0f)
		{
			for (int i = 0; i < Enemy.Length; i++) {
				
				Enemy1Mover enemyScripts = Enemy [i].GetComponent<Enemy1Mover> ();
				if (enemyScripts.points <= reinforce) {
					selectedEnemies.Add (Enemy [i]);
				}

			}
				reinforce = reinforce + 0.1f;
				spawnPoint = spawnPoints [Random.Range (0, 3)];




			Instantiate (selectedEnemies [Random.Range (0, selectedEnemies.Count)], spawnPoint.transform.position, spawnPoint.transform.rotation);
				Timer = Random.Range (2, 5);
			selectedEnemies.Clear ();

		

		}

		if (CredTimer <= 0f)
		{
			Credits += 1;
				


			CredTimer = 2;
		}

		if (StationHealth <= 0){

			StationHealth = 0;
		}

		shieldtxt = ShieldText.GetComponent<Text>();
		shieldtxt.text="Shields: " + StationHealth + "%";



		hullTxt = HullText.GetComponent<Text> ();
		hullTxt.text = "Hull: " + HullHealth + "%";



		shieldRecharge -= Time.deltaTime;
		if (shieldRecharge <= 0f && StationHealth < 100) {
			StationHealth = StationHealth + 5;
			shieldRecharge = 5;

		}

		if (StationHealth > 100) {
			StationHealth = 100;
		}


		UpdateCredits ();



		if (HullHealth <= 0.0F) {
			SceneManager.LoadScene("Menu");
		}
		
	}


	public void UpdateCredits() {

		CredText.text = "Credits: " + Credits;


	}






}
